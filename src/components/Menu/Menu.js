import React from 'react';
import { Navbar, Nav, NavDropdown, Image } from 'react-bootstrap';
import logo from '../../images/logo.png';



export default function Menu() {
    return (
        
            <Navbar  id="navbar"  expand="lg">
                <Navbar.Brand href="/"> <Image src = {logo} fluid /> </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/about">About</Nav.Link>
                    <NavDropdown title="Proline Coatings" href="/prolinecoatingsroot" id="proline-coating">
                        <NavDropdown.Item href="/color">Color</NavDropdown.Item>
                        <NavDropdown.Item href="/tools">Tools</NavDropdown.Item>
                        <NavDropdown.Item href="/proliancoatings">Proline Coatings</NavDropdown.Item>
                        {/* <NavDropdown.Divider /> */}
                        <NavDropdown.Item href="/sealer">Sealer</NavDropdown.Item>
                    </NavDropdown>
                    <NavDropdown title="Decorated concrete" href = "/decorativeconcrete"id="concreto-decorado">
                        <NavDropdown.Item  href="/accessories">Accessories</NavDropdown.Item>
                        <NavDropdown.Item  href="/colordcsc">Color</NavDropdown.Item>
                        <NavDropdown.Item  href="/edgeandformliners">Edge Form Liners</NavDropdown.Item>
                            {/* <NavDropdown.Divider /> */}
                        <NavDropdown.Item href="/gemtextsc">GEM TEX</NavDropdown.Item>
                        <NavDropdown  drop={'right'} href = "/finishingtools"  title="Finishing Tools" >
                            <NavDropdown.Item href="/accesoriesforcoatings" >Accesories for coatings</NavDropdown.Item>
                            <NavDropdown.Item href="/fsadapters" >Adapters</NavDropdown.Item>
                            <NavDropdown.Item href="/fbrooms" >Brooms</NavDropdown.Item>
                            <NavDropdown.Item href="/fscupwheels" >Cup wheels</NavDropdown.Item>
                            <NavDropdown.Item href="/fprotectionitems" >Protection Items</NavDropdown.Item>
                            <NavDropdown.Item href="/fsedgers" >Edgers</NavDropdown.Item>
                            <NavDropdown.Item href="/fsflatwirebrooms" >Flat wire brooms</NavDropdown.Item>
                            <NavDropdown.Item href="/fsfresno" >Fresno</NavDropdown.Item>
                            <NavDropdown.Item href="/fsgroovers" >Groovers</NavDropdown.Item>
                            <NavDropdown.Item href="/fshandles" >Handles</NavDropdown.Item>
                            <NavDropdown.Item href="/fskneepad" >Knee pad</NavDropdown.Item>
                            <NavDropdown.Item href="/fsmeasurer" >Measurer</NavDropdown.Item>
                            <NavDropdown.Item href="/fsmixers" >Mixers </NavDropdown.Item>
                            <NavDropdown.Item href="/fsprotectiveitems" >Protective items </NavDropdown.Item>
                            <NavDropdown.Item href="/fspullcretes" >Pull cretes</NavDropdown.Item>
                            <NavDropdown.Item href="/fsrollertamp" >Roller tamp</NavDropdown.Item>
                            <NavDropdown.Item href="/fsrollers" > Rollers</NavDropdown.Item>
                            <NavDropdown.Item href="/fsscrapers" >Scrapers</NavDropdown.Item>
                            <NavDropdown.Item href="/fsspeedform" >Speedform</NavDropdown.Item>
                            <NavDropdown.Item href="/fssprayers" >Sprayers</NavDropdown.Item>
                            <NavDropdown.Item href="/fsspeedform" >Speed Form</NavDropdown.Item>
                            <NavDropdown.Item href="/fssquegee" >Squeegee</NavDropdown.Item>
                            <NavDropdown.Item href="/fstrowelsandbullfloats" >Trowels & bull floats</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown.Item href="/tables">Tables</NavDropdown.Item>
                        <NavDropdown  drop={'right'} href="/moldsandtextures" title="Molds and textures" >
                            <NavDropdown.Item href="/woodplanks" >Wood planks</NavDropdown.Item>
                            <NavDropdown.Item href="/tumbledtravertine" >Tumbled travertine</NavDropdown.Item>
                            <NavDropdown.Item href="/tablemold" >Table mold</NavDropdown.Item>
                            <NavDropdown.Item href="/squaretile" >Square tile</NavDropdown.Item>
                            <NavDropdown.Item href="/slate" >Slate</NavDropdown.Item>
                            <NavDropdown.Item href="/seamlessskin" >Seamless Skin</NavDropdown.Item>
                            <NavDropdown.Item href="/seamlessdesingn" >Seamless desingn</NavDropdown.Item>
                            <NavDropdown.Item href="/randomstone" >Random stone</NavDropdown.Item>
                            <NavDropdown.Item href="/medallions" >Medallions</NavDropdown.Item>
                            <NavDropdown.Item href="/handsculptedaccentpieces" >Hand sculpted accent pieces</NavDropdown.Item>
                            <NavDropdown.Item href="/europeanfan" >European fan</NavDropdown.Item>
                            <NavDropdown.Item href="/cobblestone" >Cobble stone</NavDropdown.Item>
                            <NavDropdown.Item href="/brickpatterns" >Brick patterns</NavDropdown.Item>
                            <NavDropdown.Item href="/borderart" >Border art</NavDropdown.Item>
                            <NavDropdown.Item href="/bandtools" >Band tools</NavDropdown.Item>
                            <NavDropdown.Item href="/ashlar" >Ashlar</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown.Item href="/complementaryproducts">Complementary products</NavDropdown.Item>
                        <NavDropdown.Item href="/sealers">Sealers</NavDropdown.Item>
                        <NavDropdown.Item href="/stain">Stain</NavDropdown.Item>
                    </NavDropdown>
                    <Nav.Link href="/contact">Contact</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
    )
}
