import React from 'react'
import {Container, Image} from 'react-bootstrap'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

import chapinLogo from '../../images/inicio/Carroucel/Chapin_Logo.png'
import logo from '../../images/inicio/Carroucel/logo.png'
import marshalltown from '../../images/inicio/Carroucel/marshalltown-primary.png'
import midwestrake from '../../images/inicio/Carroucel/MidwestRake.jpg'
import syntec from '../../images/inicio/Carroucel/syntec-logo-1.png'
import trends from '../../images/inicio/Carroucel/treds.jpg'
import troxell from '../../images/inicio/Carroucel/Troxell.jpg'
import wooster from '../../images/inicio/Carroucel/Wooster-Brush.jpg'

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1
  }
};

export default function MultiCarroucel() {
    return (
        <Container>
            <Carousel 
                swipeable={false}
                draggable={false}
                showDots={true}
                responsive={responsive}
                autoPlay = {true}
                ssr={true} // means to render carousel on server-side.
                infinite={true}
                autoPlaySpeed={1500}
                keyBoardControl={true}
                customTransition="all .5"
                transitionDuration={500}
                containerClass="carousel-container"
                removeArrowOnDeviceType={["tablet", "mobile"]}
                dotListClass="custom-dot-list-style"
                itemClass="carousel-item-padding-40-px"
            >
                <Image  src={chapinLogo} width = {150} /> 
                <Image  src={logo} width = {150} height = {75} /> 
                <Image  src={marshalltown} width = {150} /> 
                <Image  src={midwestrake} width = {150} /> 
                <Image  src={syntec}  width = {150}/> 
                <Image  src={trends} width = {150} /> 
                <Image  src={troxell} width = {150} height = {75} /> 
                <Image  src={wooster} width = {150} /> 
            </Carousel>
        </Container>
    )
}
