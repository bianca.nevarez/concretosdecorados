import React from 'react';
import {Container} from 'react-bootstrap';
import {  TelephoneFill, Clock, Envelope, Facebook, Instagram, Whatsapp } from 'react-bootstrap-icons';

export default function Footer() {
    return (
        <div className = "main-footer">
            <div className = "container footerWrapper">
                <div id="footer1" className = "row">
                    <div id="footerItem1" className = "footerItem col-md-3 col-sm-6">
                        <h4>Products</h4>
                        <ul className="list-unstyled" >
                            <li>Color Hardener</li>
                            <li>Gem Tex</li>
                            <li>Reveal</li>
                            <li>Oxidizing Acid</li>
                            <li>EZ Accent</li>
                            <li>EZ Tique</li>
                        </ul>
                    </div>
                    <div id="footerItem2" className = "footerItem col-md-3 col-sm-6">
                        <h4>Systems</h4>
                        <ul className="list-unstyled" >
                            <li>Stamped Concrete</li>
                            <li>Rusty Concrete</li>
                            <li>Micro Thin Polished </li>
                            <li>DYE</li>
                            <li>Brushed Resurfacer</li>
                            <li>Color Renewal </li>
                            <li>Maintenance</li>
                        </ul>
                    </div>
                    <div id="footerItem3" className = "footerItem col-md-3 col-sm-6">
                        <h4>Contact</h4>
                        <ul className="list-unstyled" >
                            <li> Tel: 760-659-6775</li>
                            {/* <li> Whatsapp: (664) 440 0717</li> */}
                            <li> ORDERS@DCUVISTA.COM </li>
                        </ul>
                    </div>
                    <div id="footerItem4" className = "footerItem col-md-3 col-sm-6">
                        <h4>Another resources</h4>
                        <ul className="list-unstyled" >
                            <a href="/downloads"><li>Downloads</li> </a>
                            <li>Seminars</li>
                            
                        </ul>
                    </div>
                </div>
                <div className = "footer-bottom">
                    <Container id="headerFooter">
                        <p className = "text-xs-center">
                            &copy;{new Date().getFullYear() } DECORATIVE CONCRETE UNLIMITED, INC.

                        </p>
                    </Container>
                    <Container id="rights">

                    </Container>
                </div>
                
            </div>
        </div>
    )
}
