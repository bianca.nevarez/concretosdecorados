import React from 'react'
import {Container } from 'react-bootstrap'
import {  TelephoneFill, Clock, Envelope, Facebook, Instagram, Whatsapp } from 'react-bootstrap-icons';

import './Contact.scss';

export default function Contact() {
    return (
        <Container className =  "contact fullbigWrapper">
            <Container className ="section contactrapper">
                <Container className = "content phoneWrapper">

                    <Container className = "phoneContent">
                        <Container className = "icon homeIcon">
                            <TelephoneFill className="redIcon" color = "red"/> 
                        </Container>
                        <Container className = "name">
                            Phone number
                        </Container>
                        <Container className = "value phonetext">
                            760-659-6775
                        </Container>
                    </Container>

                </Container>
                <Container className = "content scheduleWrapper">
                    <Container className = "data">
                        <Container className = "icon homeIcon">
                            <Clock className="redIcon" color = "red" />
                        </Container>
                        <Container className = "name">
                            Schedule
                        </Container>
                        <Container className = "value scheduletext">
                            MON-FRI 7:00 am – 4:00 pm, SAT 8:00 am – 12:00 pm
                        </Container>
                    </Container>
                </Container>
                <Container className = "content emailWrapper">
                    
                    <Container className = "data">
                        <Container className = "icon homeIcon">
                            <Envelope className="redIcon" color = "red"/>
                        </Container>
                        <Container className = "name">
                            Email 
                        </Container>
                        <Container className = "value emailtext">
                            ORDERS@DCUVISTA.COM
                        </Container>
                    </Container>
                </Container>

                <Container>
                    <Container className = "content social">
                        <Facebook className="homeSocialIcon"/> 
                        <Instagram className="homeSocialIcon"/>
                        <Whatsapp className="homeSocialIcon"/>
                    </Container>    
                </Container>



            </Container>
            
        </Container>
    )
}
