import React from 'react';
import {Container}from 'react-bootstrap';

import './Header.scss';

export default function Header() {
    return (
        <div className="header coolHeader"> 
            <div id="tabHeader">

            </div>
            <Container> 
                <Container className="superHeaderText"> Distribuidor Exclusivo Proline</Container>
            </Container>
            <Container className = "header__Links">
                <Container id="notLast" className="superHeaderText" > <a  className="superHeaderText" href="/downloads"> Downloads </a> </Container>
                <Container id="notLast" className="superHeaderText" > Seminarios </Container>
                <Container id="lastTab" className="superHeaderText" > Preguntas Frecuentes </Container>
            </Container>
            <div id="tabHeader">

            </div>
        </div>
    )
}
