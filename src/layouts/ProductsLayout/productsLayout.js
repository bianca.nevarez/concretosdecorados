import React from 'react';
import {  Container, Form, Button } from 'react-bootstrap';
import {  TelephoneFill } from 'react-bootstrap-icons';
import Menu from '../../components/Menu';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import './productsLayout.scss';

const  ProductsLayout  = (props) =>  {

   
    const sendMail = ( e ) => {
        alert("Email sended success")
    }

    const { children } = props
    
    return (
        <>
            <Container className ="products_layout">
                <Header/>
                <Menu/>
                <Container className = "content">
                    <Container className = "contact">
                        <Container >
                            <Container className="contactWrapper">
                                <Container className="contactTitle">
                                    Contact
                                </Container>
                            </Container>
                            <Container className="phoneContainer">
                                <TelephoneFill/> <span id="phoneFormat">(661) 104 23 78</span>
                            </Container>

                            <Container className = "mailto">
                                <Form onSubmit={()=>{}}>

                                    <Form.Group id="emailPart" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control type="email" placeholder="name@example.com" />
                                    </Form.Group>

                                    <Form.Group id="namePart" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control type="text" placeholder="full name" />
                                    </Form.Group>
                                    
                                    <Form.Group id="MessagePart" controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Message</Form.Label>
                                        <Form.Control as="textarea" rows={3} />
                                    </Form.Group>
                                    
                                </Form>
                                <Button 
                                    id="SendContactBtn" 
                                    onClick = {sendMail}    
                                > 
                                    Send 
                                </Button>
                            </Container>
                        </Container>
                    </Container> 
                    <Container className = "col-xs-12  col-md-9  col-md-push-3">
                        {children}
                    </Container>
                 
                </Container>
                <Footer/>
            </Container>
        </>
    )
}
export default ProductsLayout;