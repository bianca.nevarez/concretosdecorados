import React from 'react';
import {  Container } from 'react-bootstrap';

import Menu from '../../components/Menu';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import './layout.scss';


const  Layout = (props) =>  {
    const { children } = props
    console.log(children)
    return (
            <div className ="layout">
                <Header/>
                <Menu/> 
                <div>
                {children}
                </div>
                <Footer/>
            </div>
    )
}
export default Layout;