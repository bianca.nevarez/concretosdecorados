import React from 'react'
import {Image } from 'react-bootstrap'
import  ProductsLayout  from '../layouts/ProductsLayout'
//import Image from '../components/Image'
import workinprogress from '../images/workinprogress.jpg'
export default function galery() {
    return (
        <ProductsLayout>
            <Image  src={workinprogress} fluid />
        </ProductsLayout>
    )
}
