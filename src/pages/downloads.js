import React from 'react'
import ProductsLayout from '../layouts/ProductsLayout'
import { FileEarmarkArrowDown } from 'react-bootstrap-icons';
import polyHdpartA from "../files/polyHdpartA.pdf" 
import polyHdPartB from "../files/PolyHdPartB.pdf"
import PolyHdTintedPartA65 from "../files/PolyHdTintedPartA65.pdf"
import PolyHdTintedPartB65 from "../files/PolyHdTintedPartB65.pdf"
import decoFlakeSds65 from "../files/DECOFLAKESDS65.pdf"
import duraAccentColorSds from "../files/DURAACCENTCOLORSDS.pdf"
import duraAntiqueReleaseSds2019 from "../files/DURAANTIQUERELEASESDS2019.pdf"
import duraCleanSds from "../files/DURACLEANSDS.pdf"
import duraColorHardenerSdS from "../files/DURACOLORHARDENERSDS65.pdf"
import duraEarlySeal700VocSds65 from "../files/DURAEARLYSEAL700VOCSDS65.pdf"
import duraEcoSealSds  from "../files/DURAECOSEALSDS.pdf"
import duraEztiqueSds from "../files/DURAEZTIQUESDS.pdf"
import duraIntegralSdsV6 from "../files/DURAINTEGRALSDSv613.pdf"
import duraLiquidReleaseCaComSds from "../files/DURALIQUIDRELEASECACOMSDS.pdf"
import duraGripSdsV9 from "../files/DURAGRIPSDSv913.pdf"
import duraStainAcidSdsRev65 from "../files/DURASTAINACIDSDSREV65.pdf"
import durathane600ABSdsBoth from "../files/DURATHANE600ABSDSBOTH.pdf"
import elastrometricSds2019 from "../files/ELASTOMERICSDS2019.pdf"
import epoxy100aGhsSds  from "../files/EPOXY100AGHSSDS.pdf"
import epoxy100BGhsSds from "../files/EPOXY100BGHSSDS.pdf"
import epoxyH20PartA from "../files/EPOXYH20PARTA.pdf"
import epoxyMetallicAGhsSds from "../files/EPOXYMETALLICAGHSSDS.pdf"
import epoxyMetallicBGhsSds from "../files/EPOXYMETALLICBGHSSDS.pdf"
import formMoldReleaseSds from "../files/FORMMOLDRELEASESDS.pdf"
import gemTexSds65 from "../files/GEMTEXSDS65.pdf"
import metallicPigmentSds from "../files/MetallicPigmentSDS.pdf"
import natureShield100600Sds65 from "../files/NATURESHIELD100600SDS65.pdf"
import proColorPackSds from "../files/PROCOLORPACKSDS.pdf"
import proCastSds from "../files/PROCASTSDS.pdf"
import proStainPart1Sds from "../files/PROSTAINPART1SDS.pdf"
import proStainPart2Sds from "../files/PROSTAINPART2SDS.pdf"
import proStainPigmentSds from "../files/PROSTAINPIGMENTSDS.pdf"
import proWaxSds from "../files/PROWAXSDS.pdf"
import probondSds65 from "../files/PROBONDSDS65.pdf"
import profastpatchSds65 from "../files/PROFASTPATCHSDS65.pdf"
import promicronSds65 from "../files/PROMICROSDS65.pdf"
import prostampSds65 from "../files/PROSTAMPSDS65.pdf"
import prosurfacerSds65 from "../files/PROSURFACERSDS65.pdf"
import protextureSds65 from "../files/PROTEXTURESDS65.pdf"
import rapidRepairPartA from "../files/RAPIDREPAIRPartA.pdf"
import rapidRepairPartB from "../files/RAPIDREPAIRPartB.pdf"
import reveal35Sds from "../files/Reveal35Sds.pdf"
import vaporBlockASds from "../files/VAPORBLOCKASDS.pdf"
import vaporBlockBSds from "../files/VAPORBLOCKBSDS.pdf"

const downloads = () => {
    return (
        <ProductsLayout>
            <div>
                <h1>Downloads</h1>
            </div>
            <div>
                <div>
                    <h2>Technical data sheet</h2>
                    <a href={polyHdpartA} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>350 POLY HD - PART A++</span>
                        </div>
                    </a>
                    <a href={polyHdPartB} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>350 POLY HD - PART B++ </span>
                        </div>
                    </a>
                    <a href={PolyHdTintedPartA65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span> 350 POLY HD - PART A-65++ </span>
                        </div>
                    </a>
                    <a href={PolyHdTintedPartB65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>350 POLY HD TINTED - PART B-65++</span>
                        </div>
                    </a>
                    <a href={decoFlakeSds65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DECO FLAKE SDS-65++ </span>
                        </div>
                    </a>
                    <a href={duraAccentColorSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA ACCENT COLOR SDS++ </span>
                        </div>
                    </a>
                    <a href={duraAntiqueReleaseSds2019} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA ANTIQUE RELEASE SDS 2019++ </span>
                        </div>
                    </a>
                    <a href={duraCleanSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA CLEAN SDS++</span>
                        </div>
                    </a>
                    <a href={duraColorHardenerSdS} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA COLOR HARDENER SDS-65++ </span>
                        </div>
                    </a>
                    <a href={duraEarlySeal700VocSds65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA EARLY SEAL 700 VOC SDS-65++ </span>
                        </div>
                    </a>
                    <a href={duraEcoSealSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA ECO SEAL SDS++ </span>
                        </div>
                    </a>
                    <a href={duraEztiqueSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA EZ-TIQUE SDS++ </span>
                        </div>
                    </a>
                    <a href={duraIntegralSdsV6} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA INTEGRAL SDS v6.13++ </span>
                        </div>
                    </a>
                    <a href={duraLiquidReleaseCaComSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA LIQUID RELEASE CA COM SDS+ </span>
                        </div>
                    </a>
                    <a href={duraGripSdsV9} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA-GRIP SDS v9-13++ </span>
                        </div>
                    </a>
                    <a href={duraStainAcidSdsRev65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURA-STAIN - ACID SDS REV-65++ </span>
                        </div>
                    </a>
                    <a href={durathane600ABSdsBoth} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>DURATHANE 600 A+B SDS BOTH==== </span>
                        </div>
                    </a>
                    <a href={elastrometricSds2019} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>ELASTOMERIC SDS 2019++ </span>
                        </div>
                    </a>
                    <a href={epoxy100aGhsSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>EPOXY 100-A GHS SDS++</span>
                        </div>
                    </a>
                    <a href={epoxy100BGhsSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>EPOXY 100-B GHS SDS++</span>
                        </div>
                    </a>
                    <a href={epoxyH20PartA} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>EPOXY H20 PART A++</span>
                        </div>
                    </a>
                    <a href={epoxyMetallicAGhsSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>EPOXY METALLIC-A GHS SDS++ </span>
                        </div>
                    </a>
                    <a href={epoxyMetallicBGhsSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>EPOXY METALLIC-B GHS SDS++ </span>
                        </div>
                    </a>
                    <a href={formMoldReleaseSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>FORM _ MOLD RELEASE SDS++ </span>
                        </div>
                    </a>
                    <a href={gemTexSds65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>GEM-TEX SDS-65++ </span>
                        </div>
                    </a>
                    <a href={metallicPigmentSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>Metallic Pigment SDS++</span>
                        </div>
                    </a>
                    <a href={natureShield100600Sds65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>NATURE SHIELD 100-600 SDS-65++</span>
                        </div>
                    </a>
                    <a href={proColorPackSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PRO COLOR PACK SDS++</span>
                        </div>
                    </a>
                    <a href={proCastSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PRO-CAST SDS++ </span>
                        </div>
                    </a>
                    <a href={proStainPart1Sds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PRO-STAIN PART 1 SDS++</span>
                        </div>
                    </a>
                    <a href={proStainPart2Sds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PRO-STAIN PART 2 SDS++ </span>
                        </div>
                    </a>
                    <a href={proStainPigmentSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PRO-STAIN PIGMENT SDS++</span>
                        </div>
                    </a>
                    <a href={proWaxSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PRO-WAX SDS++</span>
                        </div>
                    </a>
                    <a href={probondSds65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PROBOND SDS-65++ </span>
                        </div>
                    </a>
                    <a href={profastpatchSds65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PROFASTPATCH SDS-65++</span>
                        </div>
                    </a>
                    <a href={promicronSds65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PROMICRO SDS-65++ </span>
                        </div>
                    </a>
                    <a href={prostampSds65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PROSTAMP SDS-65++ </span>
                        </div>
                    </a>
                    <a href={prosurfacerSds65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PROSURFACER SDS-65++ </span>
                        </div>
                    </a>
                    <a href={protextureSds65} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>PROTEXTURE SDS-65++</span>
                        </div>
                    </a>
                    <a href={rapidRepairPartA} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>RAPID REPAIR Part A++ </span>
                        </div>
                    </a>
                    <a href={rapidRepairPartB} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>RAPID REPAIR Part B++</span>
                        </div>
                    </a>
                    <a href={reveal35Sds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>REVEAL #3 _ #5 SDS++</span>
                        </div>
                    </a>
                    <a href={vaporBlockASds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>VAPOR BLOCK-A  SDS++</span>
                        </div>
                    </a>
                    <a href={vaporBlockBSds} target="_blankc">
                        <div>
                            <FileEarmarkArrowDown />
                            <span>VAPOR BLOCK-B  SDS++</span>
                        </div>
                    </a>
                </div>
            </div>
        </ProductsLayout>
    )
}



export default downloads
