import React from 'react'
import {Image, Container } from 'react-bootstrap'
import Layout from '../layouts/Layout';
import "./about.scss"

//import Image from '../components/Image'
import workinprogress from '../images/inicio/rotatingAshlarItalianSlate.jpg'
export default function about() {
    return (
        <Layout id="fullAboutWrapper" className= "about" >

            <div id="UsBanner">
                <h1>Nosotros</h1>
                <span id="subTitle">Quiénes somos y qué hacemos</span>
            </div>
            <Container id="CategoryMenu">
                <span class="NotCurrent" >DCU México</span> / <span class="Current">Nosotros</span>
            </Container>

            <Container id="About_container" className = "image">
                <Image id="AboutImg" width={450}  style={ { float: "left" }} src={workinprogress} fluid />
                
                <div className="TextClass" id="AboutText">
                    <p>We are a company dedicated to the commercialization of products for decorated concrete.</p>
                    <br></br>

                    <p>Distributors of PROLINE, a leading company in the United States of America in the </p>
                    <p>manufacture of materials for decorated concrete and that is at the forefront of the </p>
                    <p>latest trends worldwide in finishes for decorative concrete. At DCU Mexico we are </p>
                    <p>committed to constant training and updating in new trends and products that are coming </p>
                    <p>to the market, in order to offer our clients a simpler, cheaper, modern and durable solution.</p>
                    <br></br>

                    <p>Our products are manufactured in the USA, with t he highest quality in the market </p>
                    <p>Our products are manufactured in the USA, with the highest quality in the market </p>
                    <p>and at very competitive prices. </p>
                </div>
            </Container>
            <Container id="secondWrapper" className="cultura"> 
                <Container id="MissionWrapper">
                    <h2>
                        Missión
                    </h2>

                    <div className="TextClass" id="MissionText">
                        <p>Our central commitment is the satisfaction of the needs and expectations of the client, supplying products and systems of the most advanced technology for decorated concrete, guaranteeing its quality. Avant-garde designs and affordable prices to keep us as market leaders.</p>
                    </div>
                </Container>

                <Container id="VisionWrapper">
                    <h2>
                        Visión
                    </h2>
                    <div className="TextClass" id="VisionText">
                        <p>Our central commitment is the satisfaction of the needs and expectations of the client, supplying products and systems of the most advanced technology for decorated concrete, guaranteeing its quality. Avant-garde designs and affordable prices to keep us as market leaders.</p>
                    </div>
                </Container>
                <Container id="TargetWrapper" >
                    <h2>
                        Target
                    </h2>
                    <div className="TextClass" id="TargetText">
                        <p>To maximize the satisfaction of our clients, providing them with the best quality of products in the market.</p>
                    </div>
                </Container>
            </Container>
            
        </Layout>
    )
}
