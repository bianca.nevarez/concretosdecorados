import React from "react"
//import { Link } from "gatsby"

import {Container,  Carousel, Card, Button } from 'react-bootstrap'

import Contact from '../components/Contact'
import MultiCarroucel from '../components/MultiCarroucel'
import Layout from '../layouts/Layout';
import img1 from '../images/inicio/ACCENTCOLOR.jpg'
import img2 from '../images/inicio/ARIZONAFLAGSTONE1.jpg'
import img3 from '../images/inicio/BOARDWALK.jpg'
import concretoStampado from '../images/inicio/ROMANSKIN4.jpg'
import DYE from '../images/inicio/rotatingAshlarItalianSlate.jpg'
import polishedMicrothin from '../images/inicio/ROTATINGROMANASHLAR.jpg'
import './index.scss'


const IndexPage = () => (
  <Layout>
      <div className="home">
        <div id="carouselWrapper"> 
            <Carousel>
                <Carousel.Item>
                <img
                    className="d-block w-100 carrouselimg"
                    src={img1}
                    height = { 350 }
                    width = { 800 }
                    alt="First slide"
                />
                <Carousel.Caption>
                    {/* <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p> */}
                    <Contact/>
                </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                <img
                    className="d-block w-100 carrouselimg"
                    src={img2}
                    height = { 350 }
                    width = { 800 }
                    alt="Second slide"
                />
            
                <Carousel.Caption>
                    {/* <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> */}
                     <Contact/>
                </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                <img
                    className="d-block w-100 carrouselimg"
                    src= {img3}
                    height = { 350 }
                    width = { 800 }
                    alt="Third slide"
                />
            
                <Carousel.Caption>
                    {/* <h3>Third slide label</h3>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p> */}
                     <Contact/>
                </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </div>
        
        <Container className="section">
            <Container className = "item cardWrapper">
                <Card  className ="card oneCard">
                    <Card.Img class="cardImg" variant="top" src={concretoStampado} />
                    <Card.Body>
                        <Card.Title>Stamped Concrete</Card.Title>
                        <Card.Text className="CardText">
                            Stamped concrete is the result of adding color and texture to concrete 
                            that is in a fresh state, in order to resemble the appearance of materials 
                            such as: stones, slabs, terracotta, bricks, ... 
                        </Card.Text>
                        <Button className="seeMorebtn" variant="primary">SEE MORE...</Button>
                    </Card.Body>
                </Card>
                <Card  className ="card oneCard">
                    <Card.Img class="cardImg" variant="top" src={DYE} />
                    <Card.Body>
                        <Card.Title>DYE</Card.Title>
                        <Card.Text className="CardText">
                            ULTRA SURFACE DYE or better known as concrete stain, is mixed with acetone to 
                            create a wide variety of translucent colors for the coloring of polished 
                            concrete or micro-thin concrete, ... 
                        </Card.Text>
                        <Button className="seeMorebtn" variant="primary">SeEE MORE...</Button>
                    </Card.Body>
                </Card>
                <Card  className ="card oneCard">
                    <Card.Img class="cardImg" variant="top" src={polishedMicrothin} />
                    <Card.Body>
                        <Card.Title>Polished Micro-thin </Card.Title>
                        <Card.Text className="CardText">
                            Pro Microtopping, better known as micro-thinning, is a gray colored mortar made 
                            from polymers, chemical additives and cement, which allows it to be spread and 
                            POLISHED on existing concrete. It was …  
                        </Card.Text>
                        <Button className="seeMorebtn" variant="primary">SeEE MORE...</Button>
                    </Card.Body>
                </Card>
            </Container>
            
            
        </Container>
        <Container className = "galeria">
            
        </Container>
        <Container className = "bottomredheader">             
            <Container className = "bottomheadertext">
                <h2>Our Commitment and Value </h2>
                <br/>Our commitment is to supply advanced technology products and systems for decorated concrete, 
                <br/> thus meeting the needs and expectations of the client satisfactorily to 
                <br/> maintain ourselves as leaders in the market. 
            </Container>
        </Container>
        <Container className = "carroucel">
            <MultiCarroucel/>
            
        </Container>
    </div>  
  </Layout>
)

export default IndexPage
