import React from 'react'
// import loadable from '@loadable/component';
import ProductsLayout from '../../layouts/ProductsLayout'
import {Container, Card, Button } from 'react-bootstrap'
import "./core.scss";

// const Image = loadable( () => import ('../../components/Image'));

export default function core(props) {
    // console.log(props)
    const { pageContext } = props
    // console.log( pageContext )
    return (
        <ProductsLayout className = "core">
            <Container className = "section">
                <Container  className = "items"  >
                    {pageContext.SubCategory &&  pageContext.SubCategory.map( ( item ) => (
                        <Card style={{ width: '18rem' }} className ="card">
                            {/* <Image fileName = {item.image} alt="img" /> */}
                            <Card.Img variant="top" src={item.image} />
                            <Card.Body>
                                <Button variant="primary">See more</Button>
                            </Card.Body>
                        </Card>
                    ))}
                </Container> 
            </Container> 
        </ProductsLayout>
    )
}
