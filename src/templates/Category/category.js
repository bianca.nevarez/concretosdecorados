import React from 'react'
// import loadable from '@loadable/component';
import {Link} from 'gatsby'
import ProductsLayout from '../../layouts/ProductsLayout'
import {Container, Card, Button } from 'react-bootstrap'
import "./category.scss"

// const Image = loadable( () => import ('../../components/Image'));

export default function Category(props) {
    console.log(props)
    const { pageContext } = props
    console.log( pageContext )

    return (
        <ProductsLayout className = "category">
            
            <Container className = "section">
                <Container  className = "items" >
                    {pageContext.item &&  pageContext.item.map( ( item ) => (
                        <Card style={{ width: '18rem' }} className ="card">
                            {/* <Image fileName = {item.image} alt="img" /> */}
                            <Card.Img variant="top" src={item.image} />
                            <Card.Body>
                                
                                <Link 
                                    to = { (typeof item.urlpath != 'undefined' && item.urlpath != null )? "/" + item.urlpath : "/" }
                                >
                                    <Button variant="primary">See more</Button>
                                </Link>
                            </Card.Body>
                        </Card>
                    ))}
                </Container>
            </Container>
        </ProductsLayout>
    )
}
