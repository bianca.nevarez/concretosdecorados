import React from 'react'
// import loadable from '@loadable/component';
// import { Container } from 'react-bootstrap'
import ProductsLayout from '../../layouts/ProductsLayout'
import {Container, Card, Button, Image } from 'react-bootstrap'

import TransformOembedToIframe from '../../utils/TransformOembedToIframe'
import "./item.scss";
import data from "../../data/categorias.json"

// const Image = loadable( () => import ('../../components/Image'));

const item = ( props ) => {
    
    const { pageContext } = props
    
    const descriptionHelper = ( description ) => {
        let fullDescription = ''
        description.map( (html) => { (typeof html != 'undefined' && html != null ) ?   fullDescription = fullDescription + html : fullDescription = fullDescription })
        return(
            <Container className = "description" dangerouslySetInnerHTML= {{ __html: TransformOembedToIframe(fullDescription) }}/>
        )
    }

    const relatedHandler = ( rel ) => {
        
        let itemsRel =[]

        data.map( (categoria ) => {
            categoria.SubCategory.map( (division) => {
               division.item.map( (product) => {
                    rel.map( (id) => {
                        if(product.id == id){
                            itemsRel.push(product)
                        }
                    }) 
               })
           })
        })
        return (
            <Container  className = "item__rel" >
                    {itemsRel &&  itemsRel.map( ( item ) => (
                        <Card style={{ width: '18rem' }} className ="card">
                            
                            <Card.Body>
                                <Card.Title>{item.product}</Card.Title>
                                
                                <Button variant="primary">See more</Button>
                            </Card.Body>
                        </Card>
                    ))}
            </Container>
        )
    }

    return (
        <ProductsLayout className = "item">
            {console.log( pageContext )}
            {console.log( )}
            <Container> 
                {/* <h1>
                    {pageContext.product}
                </h1> */}
            </Container>
            <Container className = "section">
                <Container  className = "items" xs={6} md={4} >
                    <Image src={pageContext.image} rounded  width = {"50%"}/>
                    {/* <Image fileName = {pageContext.image} alt="img" /> */}
                    <Container>
                        <h2> Description </h2>
                    </Container>
                    {pageContext.description &&  descriptionHelper(pageContext.description) } 

                </Container>
                
                {/* {pageContext.relproducts && relatedHandler (pageContext.relproducts)} */}
            </Container>  
        </ProductsLayout>
    )
}

export default item
