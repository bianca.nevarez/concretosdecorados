/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it
const fs = require('fs')
const path = require('path')
// const { paginate }  = require('gatsby-awesome-pagination')
const data = require('./src/data/categorias.json')

exports.createPages = async ({ actions })  => {
   
    const { createPage } = actions;
    const template = path.resolve('./src/templates/Category/category.js')
    const coreTemplate  = path.resolve('./src/templates/core/core.js')
    const itemItem = path.resolve('./src/templates/Item/item.js')

    data.map((category ) => { 
      var path = category.urlpath
      createPage({
        path,
        component : coreTemplate,
        context : category
      })
      category.SubCategory.map( (SubCategory ) => {
        var path = SubCategory.urlpath
        createPage({
          path,
          component : template,
          context : SubCategory
        })

        SubCategory.item.map( (item, id) => {
          var path = item.urlpath
          createPage({
            path,
            component : itemItem,
            context : item
          })
        })

      })
    })
    // const posts = await graphql(`
    //     query {
    //         allStrapiPost ( sort: { fields: createdAt, order: DESC } ) {
    //             nodes {
    //                 id
    //                 title
    //                 url
    //                 content
    //                 createdAt
    //                 seo_title
    //                 seo_description
    //             }
    //         }
    //     }
    // `);
  
    // Create your paginated pages
    // paginate({
    //   createPage, // The Gatsby `createPage` function
    //   items: posts.data.allStrapiPost.nodes, // An array of objects
    //   itemsPerPage: 5, // How many items you want per page
    //   pathPrefix: '/', // Creates pages like `/blog`, `/blog/2`, etc
    //   component: path.resolve(`src/templates/blog.js`), // Just like `createPage()`
    // })

    // posts.data.allStrapiPost.nodes.forEach((post) => {
    //     createPage({
    //         path: `/${post.url}`,
    //         component : path.resolve(`src/templates/post/post.js`),
    //         context : {
    //             data : post
    //         }
    //     })
    // });

  }